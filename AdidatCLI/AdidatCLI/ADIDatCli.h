// ADIDatCli.h
#pragma once
#include "NativeWriter.h"

using namespace System;
using namespace System::Collections::Generic;

namespace ADIDatCli
{
	// This class is a wrapper for the NativeWrite, the purpose is to convert managed and unmanaged types
	/// This class can create and write .adidat files for LabChart
	public ref class AdiDatWriter
	{
	public:
		/// Creates the file in the path. Also set up the
		AdiDatWriter(String^ path, IEnumerable<String^>^ channelNames, IEnumerable<String^>^ channelUnits, double secondsPerSample);
		~AdiDatWriter();
		/// Adds a new record to the file
		void AddRecord(
			IEnumerable<IEnumerable<float>^>^ data	// An enumerable of channel data. Each inner enumerable must have the same size.
		);
	private:
		// The native class does the actual work
		NativeWriter* _writer;
	};
}
