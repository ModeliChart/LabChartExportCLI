// Do not change the order of includes!!!
#include "stdafx.h"
#include <msclr/marshal_cppstd.h>
// Use the simpler C-Style interface
#include "ADIDatCli.h"


ADIDatCli::AdiDatWriter::AdiDatWriter(String ^ path, IEnumerable<String^>^ channelNames, IEnumerable<String^>^ channelUnits, double secondsPerSample)
{
	using namespace msclr::interop;
	// Create the native vectors
	std::vector<std::string> vecChannelNames;
	for each(auto name in channelNames)
	{
		if (name == nullptr)
		{
			vecChannelNames.push_back("no name");
		}
		else
		{
			vecChannelNames.push_back(marshal_as<std::string>(name));
		}
	}
	std::vector<std::string> vecChannelUnits;
	for each(auto unit in channelUnits)
	{
		if (unit == nullptr)
		{
			vecChannelUnits.push_back("no unit");
		}
		else
		{
			vecChannelUnits.push_back(marshal_as<std::string>(unit));
		}
	}
	// Check conistency
	if (vecChannelNames.size() != vecChannelUnits.size())
	{
		throw gcnew ArgumentException("The number of names and units does not match.");
	}

	// Create the native class
	_writer = new NativeWriter(marshal_as<std::string>(path), vecChannelNames, vecChannelUnits, secondsPerSample);
}

ADIDatCli::AdiDatWriter::~AdiDatWriter()
{
	delete(_writer);
}

void ADIDatCli::AdiDatWriter::AddRecord(IEnumerable<IEnumerable<float>^>^ data)
{
	std::vector<std::vector<float>> dataVector;
	// Copy the data into c++ vecors
	for each(auto channel in data)
	{
		std::vector<float> samples;
		for each(float sample in channel)
		{
			samples.push_back(sample);
		}
		dataVector.push_back(samples);
	}
	_writer->AddRecord(dataVector);
}

