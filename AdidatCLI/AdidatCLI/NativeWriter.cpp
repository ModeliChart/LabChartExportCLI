#include "stdafx.h"
#include "NativeWriter.h"
#include <ctime>

NativeWriter::NativeWriter(std::string path, std::vector<std::string> channelNames, std::vector<std::string> channelUnits, double secondsPerSample) :
	_fileHandle(0), _writerHandle(0)
{
	_channelCount = channelNames.size();
	if (_channelCount > 32)
	{
		throw new std::invalid_argument("Only a maximum of 32 channels is supported by the LabView data export.");
	}

	// Create the file
	std::wstring wPath(path.begin(), path.end());
	checkResult(ADI_CreateFile(wPath.c_str(), &_fileHandle));

	// Start writing
	checkResult(ADI_CreateWriter(_fileHandle, &_writerHandle));


	// Set the channels names and indo
	for (size_t channel = 0; channel < channelNames.size(); channel++)
	{
		std::wstring wName(channelNames[channel].begin(), channelNames[channel].end());
		checkResult(ADI_SetChannelName(_fileHandle, static_cast<long>(channel), wName.c_str()));
		std::wstring wUnit(channelUnits[channel].begin(), channelUnits[channel].end());
		checkResult(ADI_SetChannelInfo(_writerHandle, static_cast<long>(channel), true, secondsPerSample, wUnit.c_str(), NULL));
	}

}

NativeWriter::~NativeWriter()
{
	// Close the file
	checkResult(ADI_CloseWriter(&_writerHandle));
	checkResult(ADI_CloseFile(&_fileHandle));
}

void NativeWriter::checkResult(int code)
{
	ADIResultCode adiCode = static_cast<ADIResultCode>(code);
	if (adiCode != ADIResultCode::kResultSuccess)
	{
		wchar_t msg[MAX_ERROR_MESSAGE_SIZE];
		long msgLen;
		ADI_GetErrorMessage(adiCode, msg, MAX_ERROR_MESSAGE_SIZE, &msgLen);
		std::wstring msgWStr(msg);
		std::string msgStr(msgWStr.begin(), msgWStr.end());
		throw new std::exception(msgStr.c_str());
	}
	// If it is kResultsuccess we are all fine :)
}

void NativeWriter::AddRecord(std::vector<std::vector<float>> data)
{
	// Determine how many samples we can add
	size_t samplesToAdd = SIZE_MAX;
	for (auto channel : data)
	{
		if (channel.size() < samplesToAdd)
		{
			samplesToAdd = channel.size();
		}
	}
	// Start a new record
	time_t currentTime;
	time(&currentTime);
	checkResult(ADI_StartRecord(_writerHandle, currentTime, 0, 0));
	// Add chunks of samples parallel for all channels
	// Add the data to the record
	size_t samplesAdded = 0;
	while (samplesAdded < samplesToAdd)
	{
		// Add Optimal chunk size for each channel
		long chunkSize = static_cast<long>(samplesToAdd - samplesAdded);
		if (OPTIMAL_CHUNK_SIZE < chunkSize)
		{
			chunkSize = OPTIMAL_CHUNK_SIZE;
		}
		long samplesAddedInStep = 0;
		for (size_t channel = 0; channel < data.size(); channel++)
		{
			// Write it
			long writtenSamples = 0;
			checkResult(ADI_AddChannelSamples(_writerHandle, static_cast<long>(channel), data[channel].data() + samplesAdded, chunkSize, &writtenSamples));
			// Weird but once the chunk is written to the first channel, all other channels will retunr writtenSamples = 0 :(
			samplesAddedInStep += writtenSamples;
		}
		samplesAdded += samplesAddedInStep;
	}

	// Close the record
	checkResult(ADI_FinishRecord(_writerHandle));
	// Commit the changes
	checkResult(ADI_CommitFile(_writerHandle, 0));
}