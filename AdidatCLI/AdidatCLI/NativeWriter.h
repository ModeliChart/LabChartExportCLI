#pragma once
#include <string>
#include <vector>
#include "ADIDatCAPI.h"


// For best display in labchart medium size chunks
const long OPTIMAL_CHUNK_SIZE = 1024;
// The maximal count of characters when getting the error message
const size_t MAX_ERROR_MESSAGE_SIZE = 1024;

class NativeWriter
{
private:
	// Handles for the file and writer
	// Must stay open otherwise writing fails ?!
	ADI_FileHandle _fileHandle;
	ADI_WriterHandle _writerHandle;

	// Rememebr how many channels we use
	size_t _channelCount;
	/// Throws an exception if any error occured
	void checkResult(int code);
public:
	NativeWriter(std::string path, std::vector<std::string> channelNames, std::vector<std::string> channelUnits, double secondsPerSample);
	void AddRecord(std::vector<std::vector<float>> data);
	~NativeWriter();
};

